      *****************************************************************
      * Program name:    Data1                               
      * Original author: MYNAME                                
      *
      * Maintenence Log                                              
      * Date      Author        Maintenance Requirement               
      * --------- ------------  --------------------------------------- 
      * 01/01/08 MYNAME  Created for COBOL class         
      *                                                               
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  DATA1.
       AUTHOR. Supakit. 
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM1           PIC   999      VALUE ZEROES.
       01  NUM2           PIC   999      VALUE 15.
       01  TAX-RATE       PIC   V99      VALUE .35.
       01  CUSTOMER-NAME  PIC   X(15)    VALUE "Supakit".

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "NUM1 " NUM1
           DISPLAY "NUM2 " NUM2
           DISPLAY "TAX-RATE " TAX-RATE 
           DISPLAY "CUSTOMER-NAME " CUSTOMER-NAME 
           .
      *****************************************************************
