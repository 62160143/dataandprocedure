      *****************************************************************
      * Program name:    MYPROG                               
      * Original author: MYNAME                                
      *
      * Maintenence Log                                              
      * Date      Author        Maintenance Requirement               
      * --------- ------------  --------------------------------------- 
      * 01/01/08 MYNAME  Created for COBOL class         
      *                                                               
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  Data3.
       AUTHOR. Supakit. 
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME  PIC   X(8)  VALUE "SUPAKIT".
       01  SALE-PRICE  PIC   9(4)V99.
       01  NUM-OF-EMPLOYEES  PIC   999V99.
       01  SALARY   PIC   9999V99.
       01  COUNTY-NAME PIC X(9).
       PROCEDURE DIVISION. 
       Begin.
           DISPLAY "1 " SURNAME 
           MOVE "SMITH" TO SURNAME
           DISPLAY "2 " SURNAME 
           MOVE "FITZWILLIAM" TO SURNAME
           DISPLAY "3 " SURNAME 
           .
           DISPLAY "1 " SALE-PRICE 
           MOVE ZEROES TO SALE-PRICE
           DISPLAY "2 " SALE-PRICE 
           MOVE 25.5 TO SALE-PRICE
           DISPLAY "3 " SALE-PRICE 
           MOVE 7.533 TO SALE-PRICE 
           DISPLAY "4 " SALE-PRICE 
           MOVE 93425.158 TO SALE-PRICE
           DISPLAY "5 " SALE-PRICE 
           MOVE 128 TO SALE-PRICE
           DISPLAY "6 " SALE-PRICE 
           .
           DISPLAY "1 " NUM-OF-EMPLOYEES 
           MOVE 12.4 TO NUM-OF-EMPLOYEES 
           DISPLAY "2 " NUM-OF-EMPLOYEES 
           MOVE 745 TO NUM-OF-EMPLOYEES 
           DISPLAY  "3 " NUM-OF-EMPLOYEES 

           MOVE NUM-OF-EMPLOYEES TO SALARY
           DISPLAY "1 " SALARY 
           .
           MOVE "GALWAY" TO COUNTY-NAME
           DISPLAY COUNTY-NAME
           MOVE ALL "*/" TO COUNTY-NAME 
           DISPLAY COUNTY-NAME 
           . 

 